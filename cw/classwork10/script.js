let num1 = num2 = sign = '';
let numbers = document.querySelectorAll('.number');
let signs = document.querySelectorAll('.sign');
let result = document.querySelector('#result');
let clear = document.querySelector('#clear');
let field = document.querySelector('#output');

for (let i = 0; i < numbers.length; i++) {
    numbers[i].addEventListener('click', () => {
        num2 += event.target.dataset.number;
        field.innerHTML += event.target.dataset.number;
    });
};

for (let i = 0; i < signs.length; i++) {
    signs[i].addEventListener('click', () => {
        sign = event.target.dataset.sign;
        num1 = num2;
        num2 = '';
        field.innerHTML += event.target.dataset.sign;
    });
};

result.addEventListener('click', () => {
    switch (sign) {
        case '+':
            field.innerHTML = +num1 + +num2;
        break;
        case '-':
            field.innerHTML = +num1 - +num2;
        break;
        case '*':
            field.innerHTML = +num1 * +num2;
        break;
        case '/':
            field.innerHTML = +num1 / +num2;
        break;
    }
    num1 = '';
    num2 = field.innerHTML;
});

clear.addEventListener('click', () => {
    field.innerHTML = num1 = num2 = '';
});