let init = () => {
    let form = document.forms[0];

    let formValidation = false;

    for (let i = 0; i < form.elements.length; i++) {
        let el = form.elements[i];

        if (el.type != 'text') {
            continue;
        }

        let pattern = el.getAttribute('data-val');

        if (pattern) {
            el.onchange = validateInput;
            formValidation = true;
        }
    }
    if (formValidation) {
        form.onsubmit = validateForm;
    }
}

function validateInput () {
    let pattern = this.dataset.val;
    let msg = this.dataset.valMsg;
    let msgId = this.dataset.valMsgId;
    let value = this.value;

    let res = value.search(pattern);
    if (res == -1) {
        document.getElementById(msgId).innerHTML = msg;
        document.getElementById(msgId).className = 'show';
        this.className = 'error';
    } else {
        document.getElementById(msgId).innerHTML = '';
        document.getElementById(msgId).className = 'green';
        this.className = 'valid';
    }
}

function validateForm () {

    let invalid = false;

    for (let i = 0; i < this.elements.length; ++i) {
        let el = this.elements[i];
        if (el.type == 'text' && el.onchange != null) {
            el.onchange();
            if (el.className == 'error') {
                invalid = true;
            }
        }
    }

    if (invalid) {
        alert("Допущены ошибки при заполнении формы.");
        return false;
    }
}

init();



////////////////////////////////////////////



let outputBasket = document.querySelector('#outputBasket');
outputBasket.innerHTML = localStorage.value;



///////////////////////////////////////////



let sale = document.querySelector('#sale');
let main = document.querySelector('main');

let maxTop = main.clientHeight - 50 - 79;
let maxLeft = main.clientWidth - 150;

sale.addEventListener('mouseover', (event) => {
    event.target.style.top = `${Math.floor(Math.random() * maxTop)}px`;
    event.target.style.left = `${Math.floor(Math.random() * maxLeft)}px`;
});