let get = (id) => document.getElementById(id);

let removeBorder = (element) => {
    element.style.borderTopWidth = '0';
    element.style.borderRightWidth = '0';
    element.style.borderBottomWidth = '0';
    element.style.borderLeftWidth = '0';
};

let setSizes = (element, wid, hei) => {
    element.style.width = wid;
    element.style.height = hei;
};

let counter = get('counter');
counter.style.fontSize = '30px';

let buttonStart = get('startButton');
buttonStart.style.backgroundColor = '#98fb98';
removeBorder(buttonStart);
setSizes(buttonStart, '70px', '30px');

let buttonStop = get('stopButton');
buttonStop.style.backgroundColor = '#ffd700';
removeBorder(buttonStop);
setSizes(buttonStop, '70px', '30px');

let buttonReset = get('resetButton');
buttonReset.style.backgroundColor = '#fa8072';
removeBorder(buttonReset);
setSizes(buttonReset, '70px', '30px');



let counterMinutes = 1;
let counterSeconds = 1;
let counterMs = 1;
let intervalChangingMinutes;
let intervalChangingSeconds;
let intervalChangingMs;

let countMinutes = () => {
    if (counterMinutes == 59) {
        counterMinutes = 0;
    }
    get('minutes').innerHTML = counterMinutes;
    counterMinutes++;
}

let countSeconds = () => {
    if (counterSeconds == 59) {
        counterSeconds = 0;
    }
    get('seconds').innerHTML = counterSeconds;
    counterSeconds++;
}

let countMs = () => {
    if (counterMs == 59) {
        counterMs = 0;
    }
    get('ms').innerHTML = counterMs;
    counterMs++;
}

get('startButton').onclick = () => {
    intervalChangingMinutes = setInterval(countMinutes, 60000);
    intervalChangingSeconds = setInterval(countSeconds, 1000);
    intervalChangingMs = setInterval(countMs, 1);
}

get('stopButton').onclick = () => {
    clearInterval(intervalChangingMinutes);
    clearInterval(intervalChangingSeconds);
    clearInterval(intervalChangingMs);
}

get('resetButton').onclick = () => {
    clearInterval(intervalChangingMinutes);
    clearInterval(intervalChangingSeconds);
    clearInterval(intervalChangingMs);
    get('minutes').innerHTML = 0;
    get('seconds').innerHTML = 0;
    get('ms').innerHTML = 0;
}