const excangeRate = document.querySelector('#root');

const GBP = 32.04,
        TRY = 4.30,
        BYN = 12.20,
        EUR= 27.69;

const p1 = document.createElement('p'),
    p2 = document.createElement('p'),
    p3 = document.createElement('p'),
    p4 = document.createElement('p');

p1.innerHTML = `GBP &nbsp; &nbsp; &nbsp; ${GBP} &nbsp; &nbsp; &nbsp; ${((GBP * 85) / 100).toFixed(2)}`;
p2.innerHTML = `TRY &nbsp; &nbsp; &nbsp; ${TRY} &nbsp; &nbsp; &nbsp; ${((TRY * 85) / 100).toFixed(2)}`;
p3.innerHTML = `BYN &nbsp; &nbsp; &nbsp; ${BYN} &nbsp; &nbsp; &nbsp; ${((BYN * 85) / 100).toFixed(2)}`;
p4.innerHTML = `EUR &nbsp; &nbsp; &nbsp; ${EUR} &nbsp; &nbsp; &nbsp; ${((EUR * 85) / 100).toFixed(2)}`;

excangeRate.appendChild(p1);
excangeRate.appendChild(p2);
excangeRate.appendChild(p3);
excangeRate.appendChild(p4);