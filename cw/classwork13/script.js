let gold = document.querySelector('#XAU');
let silver = document.querySelector('#XAG');
let rupy = document.querySelector('#INR');
let dollar = document.querySelector('#NZD');
let pound = document.querySelector('#LBP');
let values = [gold, silver, rupy, dollar, pound];


let xhr = new XMLHttpRequest();

xhr.open("GET", "https://old.bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
        var data = JSON.parse(xhr.responseText);
        
        let dataFiltered = data.filter(value => value.cc == gold.dataset.currency ||
                                                value.cc == silver.dataset.currency||
                                                value.cc == rupy.dataset.currency ||
                                                value.cc == dollar.dataset.currency || 
                                                value.cc == pound.dataset.currency);

        values.forEach((price) => {
            dataFiltered.forEach((element) => {
                if (element.cc == price.dataset.currency) {
                    price.children[1].innerHTML = element.rate;
                    price.children[2].innerHTML = (1 / element.rate).toFixed(7);
                }
            });
        });
    }
}
xhr.send();