let btn = document.querySelector('#save');

btn.addEventListener('click', () => {
    btn.dataset.saved = 'true';
});

window.onbeforeunload = function() {
    if (btn.dataset.saved == 'false') {
        let message =  'Вы не сохранили данные. Покинуть страницу?';
        return message
    }
};