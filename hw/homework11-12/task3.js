let sale = document.querySelector('#sale');
let body = document.querySelector('body');

let maxTop = body.clientHeight - 50;
let maxLeft = body.clientWidth - 100;

sale.addEventListener('mouseover', (event) => {
    event.target.style.top = `${Math.floor(Math.random() * maxTop)}px`;
    event.target.style.left = `${Math.floor(Math.random() * maxLeft)}px`;
});