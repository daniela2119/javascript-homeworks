let paragraphs = document.querySelectorAll('.changeColor');
let btnR = document.querySelector('#r');
let btnG = document.querySelector('#g');
let btnB = document.querySelector('#b');

btnR.addEventListener('click', () => {
    for (let i = 0; i < paragraphs.length; i++) {
        paragraphs[i].style.color = 'red';
    }
});

btnG.addEventListener('click', () => {
    for (let i = 0; i < paragraphs.length; i++) {
        paragraphs[i].style.color = 'green';
    }
});

btnB.addEventListener('click', () => {
    for (let i = 0; i < paragraphs.length; i++) {
        paragraphs[i].style.color = 'blue';
    }
});