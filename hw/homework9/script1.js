let randomNumber = Math.ceil(Math.random() * 100);
let usersNumber;
let button = document.querySelector('button');

button.addEventListener('click', () => {
    usersNumber = document.querySelector('#usersNumber').value;
    if (randomNumber > usersNumber) {
        alert('Загаданное число больше введённого вами');
    } else if (randomNumber < usersNumber) {
        alert('Загаданное число меньше введённого вами');
    } else {
        alert('Вы угадали)');
    }
});