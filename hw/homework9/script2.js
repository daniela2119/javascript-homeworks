let button = document.querySelector('#signUp');
let inputLogin = document.querySelector('#login');
let inputPassword = document.querySelector('#password');
let message = document.querySelector('#output');

button.addEventListener('click', () => {
    if (inputLogin.value == '' && inputPassword.value == '') {
        message.innerHTML = 'Вы не заполнили поля логин и пароль';
        message.style.color = '';
        inputLogin.style.backgroundColor = '#fa8092';
        inputPassword.style.backgroundColor = '#fa8092';
    } else if (inputLogin.value == 'admin' && inputPassword.value == '12345') {
        message.innerHTML = 'Вы авторизированы';
        message.style.color = '#32cd32';
        inputLogin.style.backgroundColor = '';
        inputPassword.style.backgroundColor = '';
    };
});