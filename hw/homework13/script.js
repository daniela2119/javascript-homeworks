let output = document.querySelector('#output');
let mistake = document.querySelector('#mistake');

let xhr = new XMLHttpRequest();

xhr.open("GET", "https://old.bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200) {
        var data = JSON.parse(xhr.responseText);
        let dataFiltered = data.filter(value => value.rate > 25);



        if (dataFiltered.lenght != 0) {
            dataFiltered.forEach((value) => {
                let tr = document.createElement('tr');
                output.append(tr);

                let td1 = document.createElement('td');
                td1.innerHTML = value.txt;
                tr.append(td1);

                let td2 = document.createElement('td');
                td2.innerHTML = value.rate;
                tr.append(td2);
            
                let td3 = document.createElement('td');
                td3.innerHTML = (1 / value.rate).toFixed(7);
                tr.append(td3);
            });
        } else {
            mistake.innerHTML = 'Произошла ошибка'
        };
    };
}
xhr.send();