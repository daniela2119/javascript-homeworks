let clickButton = () => {
    let divElements = document.querySelectorAll('div');
    let body = document.querySelector('body');

    if (divElements.length < 10) {
        let divElement = document.createElement('div');
        divElement.innerHTML = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores enim exercitationem doloribus fugit iste rem.';
        body.appendChild(divElement);
    } else {
        for (let i = divElements.length; i > 0; i--) {
            let divToRemove = document.querySelector('div');
            divToRemove.parentNode.removeChild(divToRemove);
        }
    }
}